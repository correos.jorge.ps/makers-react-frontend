import { useContext } from 'react';

import { LanguageContext, LanguagesType } from '../providers/languageProvider';

export const useLanguage = () => {
    const { language, setLanguage } = useContext(LanguageContext);

    const modifyLanguage = (newLanguage: LanguagesType) => {
        setLanguage(newLanguage);
    };
    return {
        language,
        modifyLanguage,
    };
};
