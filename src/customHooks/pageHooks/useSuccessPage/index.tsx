import { useEffect, useState } from 'react';
import './index.scss';
import { useHistory, useLocation } from 'react-router-dom';
import { InventoryService } from '../../../services/inventoryService';
import { useUserData } from '../../useUserData';
import { RoleType, IRequest, IInventory } from '../../../models';
import { EntityType, UrlManager } from '../../../helpers/helpers';
import { RequestService } from '../../../services/requestService';
import useNotification from '../../useNotification';
//@ts-ignore
interface IProps {
    entity: EntityType;
    role: RoleType;
    statusModifier: number;
}

export const useSummaryPage = ({ role, entity, statusModifier }: IProps) => {
    let history = useHistory();
    let location = useLocation();
    const { person } = useUserData();
    const [data, setData] = useState<IRequest | IInventory | null>();
    const [isJustCreated, setIsJustCreated] = useState<boolean>(false);

    const { setNotificationMessageDelayed } = useNotification();

    const changeStatus = (shipping: number) => {
        if (!!person) {
            InventoryService()
                .updateShippingStatus(person.pk, shipping, statusModifier)
                .then(() => {
                    setNotificationMessageDelayed('green');
                    history.push(UrlManager(role, entity).main);
                })
                .catch(() => {
                    setNotificationMessageDelayed('red');
                });
        }
    };

    useEffect(() => {
        const { state } = location;
        if (!!state) {
            //@ts-ignore
            setData(state.data);
            //@ts-ignore
            setIsJustCreated(state.isJustCreated);
        } else {
            history.push(UrlManager(role, entity).main);
        }
    }, [history, location, entity, role]);

    const removeItem = () => {
        if (!!person && data) {
            if (entity === EntityType.Request) {
                RequestService()
                    .cancelRequest(person.pk, data.pk)
                    .then(() => {
                        setNotificationMessageDelayed('green');
                        history.push(UrlManager(role, entity).main);
                    })
                    .catch(() => {
                        setNotificationMessageDelayed('red');
                    });
            } else {
                InventoryService()
                    .deleteInventory(person.pk, data.pk)
                    .then(() => {
                        setNotificationMessageDelayed('green');
                        history.push(UrlManager(role, entity).main);
                    })
                    .catch(() => {
                        setNotificationMessageDelayed('red');
                    });
            }
        }
    };
    return {
        changeStatus,
        removeItem,
        data,
        isJustCreated,
    };
};
