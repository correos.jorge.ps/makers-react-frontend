import { useContext } from 'react';
import { NotificationContext } from '../providers/notificationProvider';
import { useIntl } from 'react-intl';

const useNotification = () => {
    const intl = useIntl();
    const { color, setColor, setNotificationMessage, notificationMessage } = useContext(NotificationContext);
    const setNotificationMessageDelayed = (colorChange: 'green' | 'red', message?: string) => {
        let text =
            colorChange === 'green'
                ? intl.formatMessage({ id: 'LABEL_ACTION_SUCCESS' })
                : intl.formatMessage({ id: 'LABEL_ACTION_ERROR' });

        if (!!message) {
            text = intl.formatMessage({ id: message });
        }
        setColor(colorChange);
        setNotificationMessage(text);
        setTimeout(() => {
            setNotificationMessage('');
        }, 4000);
    };

    return {
        color,
        setNotificationMessageDelayed,
        notificationMessage,
    };
};

export default useNotification;
