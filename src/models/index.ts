export interface IInventory {
    pk: number;
    resource: number;
    owner: number;
    batchs_object: [];
    quantity: number;
    track_batch: number;
    photo_link: string;
    shippings_object: any[];
    auto_gen_batch: number;
    created_date: string;
    pad_batch: string;
    quantity_available: number;
    resource_object: IResource;
}

export interface IRequest {
    pk: number;
    consumer: number;
    consumer_object: IPerson;
    resource: number;
    shippings: number[];
    shippings_object: any[];
    quantity: number;
    done: boolean;
    status: number;
    created_date: string;
    resource_object: IResource;
}
export interface IShipping {
    pk: number;
    uuid: string;
    status: number;
    carrier?: number;
    carrier_object?: { pk: number; role: RoleType; name: string };
    request?: number;
    request_object?: IRequest;
    created_date: string;
    quantity: number;
    last_updated_date: string;
}
export interface IResourceObject {
    pk: number;
    uuid: string;
    carrier: number;
    carrier_object: IPerson[];
}

export interface IResource {
    pk: number;
    name: string;
    sku: number;
    resource_type: ResourceType;
    spl_link: string;
    image_link?: string;
    howto_link?: string;
}

export interface IPaginatedResponse<T> {
    count: number;
    next: null;
    previous: null;
    results: T[];
}

export interface IRole {
    pk: RoleType;
    name: string;
}
export interface IPerson {
    pk: number;
    role: RoleType;
    name: string;
    role_disambiguation: number;
    region: number;
    region_object: { pk: number; name: string };
    telegram_nick: string;
    telegram_id: string | null;
    address: string;
    phone: string;
    point: { type: 'Point'; coordinates: [number, number] };
    production_capacity: number;
    machine_models: string | null;
    machine_count: number;
    logistics_need: boolean;
    user: number;
    can_set_received_status: boolean;
    can_set_send_status: boolean;
}

export enum RoleType {
    Maker = 0,
    HealthCare = 1,
    Warehouse = 2,
    AreaManager = 3,
    Transport = 4,
}
export enum ResourceType {
    Material = 0,
    Parts = 1,
    FinalProduct = 2,
}
