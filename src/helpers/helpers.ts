import { RoleType } from '../models';

export enum EntityType {
    Inventory = 0,
    Request = 1,
}
const getRoleUrl = (type: RoleType) => {
    if (type === RoleType.HealthCare) {
        return 'health';
    }
    return 'maker';
};

const getEntityUrlByRole = (type: RoleType) => {
    if (type === RoleType.HealthCare) {
        return EntityType.Request;
    }
    return EntityType.Inventory;
};

const getEntityUrl = (entity: EntityType) => {
    if (entity === EntityType.Request) {
        return 'request';
    }
    return 'inventory';
};

export const UrlManager = (type: RoleType, entity?: EntityType) => {
    if (!entity) {
        entity = getEntityUrlByRole(type);
    }
    return {
        main: `/${getRoleUrl(type)}/${getEntityUrl(entity)}/main`,
        create: `/${getRoleUrl(type)}/${getEntityUrl(entity)}/create`,
        success: `/${getRoleUrl(type)}/${getEntityUrl(entity)}/success`,
    };
};
