import React from 'react';
import { useIntl } from 'react-intl';
import { Layout } from '../../layout';
import { Button } from '../../../components/button';
import InfiniteScroll from 'react-infinite-scroll-component';
import { Spinner } from '../../../components/spinner';
import { InventoryItem } from '../../../components/card/inventoryItem';
import { ResourceType } from '../../../models';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faPlus } from '@fortawesome/free-solid-svg-icons';

interface IProps {
    pageTitle: string;
    action: () => void;
    inventory: any[];
    getNext: () => void;
    nextUrl: string | null;
    resourceType?: ResourceType;
}
export const MainInventoryContainer = ({
    pageTitle,
    action,
    inventory,
    getNext,
    nextUrl,
    resourceType = ResourceType.FinalProduct,
}: IProps) => {
    const intl = useIntl();

    return (
        <Layout>
            <div id="list">
                <div className="header">
                    <h2>{pageTitle}</h2>
                    <Button type="rounded-desk primary" action={() => action()}>
                        <FontAwesomeIcon size="lg" icon={faPlus} />
                    </Button>
                </div>
                <InfiniteScroll
                    dataLength={inventory.length}
                    next={() => getNext()}
                    hasMore={!!nextUrl}
                    scrollThreshold="600px"
                    scrollableTarget="list"
                    endMessage={<p className="noMorePost">{intl.formatMessage({ id: 'LABEL-LIST-FINISHED' })}</p>}
                    loader={
                        <div className="spinerBlock">
                            <Spinner />
                        </div>
                    }
                >
                    {inventory.map((item) => (
                        <InventoryItem key={item.pk} item={item} resourceType={resourceType} />
                    ))}
                </InfiniteScroll>
                <Button type="rounded primary" action={() => action()}>
                    <FontAwesomeIcon size="lg" icon={faPlus} />
                </Button>
            </div>
        </Layout>
    );
};
