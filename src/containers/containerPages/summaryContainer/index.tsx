import React, { useEffect } from 'react';
import { Layout } from '../../layout';
import { useIntl } from 'react-intl';
//@ts-ignore
import QRCode from 'qrcode.react';
import { Button } from '../../../components/button';
import { useUserData } from '../../../customHooks/useUserData';
import { IRequest, IInventory } from '../../../models';
import { ShippingItem } from '../../../components/card/shippingItem';
interface IProps {
    data: IRequest | IInventory | null | undefined;
    action: () => void;
    title: string | null;
    buttonConfirmLabel: string;
    subtitle: string;
    instructions?: string;
    buttonLabel: string;
    changeStatus: (arg: number) => void;
    hasQr?: boolean;
    removeItem: () => void;
}

export const SummaryContainer = ({
    action,
    title,
    subtitle,
    instructions,
    buttonLabel,
    changeStatus,
    hasQr = true,
    removeItem,
    data,
    buttonConfirmLabel,
}: IProps) => {
    const { person } = useUserData();
    const intl = useIntl();
    useEffect(() => {
        const link: any = document.getElementById('link');
        const canvas: any = document!.getElementsByTagName('canvas');
        if (canvas.length > 0) {
            link.href = canvas[0].toDataURL();
            link.download = 'qr.png';
        }
    }, [data]);

    if (!data) {
        return <div></div>;
    }
    let hasWaitingShipping = data.shippings_object.filter((item) => item.status > 0);
    return (
        <Layout>
            <div className="successPage">
                {!!title && <h2>{intl.formatMessage({ id: title })}</h2>}

                <div className="intro">
                    <h3>ID: {data.pk}</h3>
                    <span>{data.resource_object.name}</span>
                    <span>Unidades:{data.quantity}</span>
                </div>
                {hasQr && (
                    <div className="margin-xl">
                        <div className="qr-code">
                            <QRCode renderAs="canvas" value={String(data.pk)} />
                        </div>
                        <a id="link">
                            <Button type="primary small downloadQr" action={() => ({})}>
                                {intl.formatMessage({ id: 'LABEL-QR-DOWNLOAD' })}
                            </Button>
                        </a>
                        <h5>{intl.formatMessage({ id: instructions })}</h5>
                    </div>
                )}
                {instructions && data.shippings_object.length === 0 ? (
                    <h5>{intl.formatMessage({ id: subtitle })}</h5>
                ) : (
                    <div className="shippings">
                        <h4>Envíos</h4>
                        <h5>{intl.formatMessage({ id: subtitle })}</h5>
                        {data.shippings_object.map((shipping) => (
                            <ShippingItem
                                key={shipping.pk}
                                item={shipping}
                                changeStatus={(id) => changeStatus(id)}
                                buttonConfirmLabel={buttonConfirmLabel}
                            />
                        ))}
                    </div>
                )}
                {(!hasWaitingShipping || (hasWaitingShipping && hasWaitingShipping.length === 0)) &&
                    person &&
                    (person.can_set_send_status || person.can_set_received_status) && (
                        <Button type="danger small" action={() => removeItem()}>
                            {intl.formatMessage({ id: 'LABEL_SET_DELETE' })}
                        </Button>
                    )}
                <div className="buttonsContainer">
                    <Button type="full secondary" action={() => action()}>
                        {intl.formatMessage({ id: buttonLabel })}
                    </Button>
                </div>
            </div>
        </Layout>
    );
};
