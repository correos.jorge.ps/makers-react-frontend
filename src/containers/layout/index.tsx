import React, { useEffect } from 'react';
import './index.scss';
import { Header } from '../../components/header';
import Modal from '../modal';
import { useModal } from '../../customHooks/useModal';
import { motion, AnimatePresence } from 'framer-motion';
import useNotification from '../../customHooks/useNotification';
import { NotificationBar } from '../../components/notificationBar/notificationBar';

export const Layout = ({ children, hasHeader = true }: any) => {
    const { showModal, resetModal } = useModal();
    const { color, notificationMessage } = useNotification();

    const variants = {
        hidden: {
            opacity: 0,
            transition: {
                duration: 0.3,
                ease: 'easeOut',
            },
        },
        visible: {
            opacity: 1,
            transition: {
                duration: 0.3,
                ease: 'easeOut',
            },
        },
    };

    useEffect(() => {
        let isSubscribed = true;
        let body = document.getElementsByTagName('body');
        let root = document.getElementById('root');

        if (body && root && isSubscribed) {
            body[0].style.overflow = showModal.active ? 'hidden' : 'auto';
            root.style.overflow = showModal.active ? 'hidden' : 'auto';
        }
        return () => {
            let body = document.getElementsByTagName('body');
            let root = document.getElementById('root');

            if (body && root && isSubscribed) {
                body[0].style.overflow = 'auto';
                root.style.overflow = 'auto';
            }
            isSubscribed = false;
        };
    }, [showModal.active]);

    return (
        <div className="layout">
            {hasHeader && <Header />}
            <NotificationBar notificationMessage={notificationMessage} color={color} />
            <AnimatePresence initial={false}>
                <motion.main
                    className="contentLayout"
                    animate={'visible'}
                    initial={'hidden'}
                    exit="hidden"
                    variants={variants}
                >
                    {children}
                </motion.main>
            </AnimatePresence>
            <Modal open={showModal.active} close={() => resetModal()}>
                {showModal.content}
            </Modal>
        </div>
    );
};
