import React from 'react';
import { useIntl } from 'react-intl';
interface IProps {
    onConfirm: () => void;
}
export const DataProtectionModal = ({ onConfirm }: IProps) => {
    const intl = useIntl();
    return (
        <div className="modalLimit left">
            <h3>{intl.formatMessage({ id: 'LOPD_TITLE' })}</h3>
            <p>{intl.formatMessage({ id: 'LOPD_TEXT_1' })}</p>
            <p>{intl.formatMessage({ id: 'LOPD_TEXT_2' })}</p>
            <strong>{intl.formatMessage({ id: 'LOPD_TEXT_3' })}</strong>
            <p>{intl.formatMessage({ id: 'LOPD_TEXT_4' })}</p>
            <p className="title">{intl.formatMessage({ id: 'LOPD_TEXT_5' })}</p>
            <p>{intl.formatMessage({ id: 'LOPD_TEXT_6' })}</p>
            <p className="title">{intl.formatMessage({ id: 'LOPD_TEXT_7' })}</p>
            <p>{intl.formatMessage({ id: 'LOPD_TEXT_8' })}</p>
            <p className="title">{intl.formatMessage({ id: 'LOPD_TEXT_9' })}</p>
            <p>{intl.formatMessage({ id: 'LOPD_TEXT_10' })}</p>
            <p className="title">{intl.formatMessage({ id: 'LOPD_TEXT_11' })}</p>
            <p>{intl.formatMessage({ id: 'LOPD_TEXT_12' })}</p>
            <ul>
                <li>{intl.formatMessage({ id: 'LOPD_TEXT_13' })}</li>
                <li>{intl.formatMessage({ id: 'LOPD_TEXT_14' })}</li>
                <li>{intl.formatMessage({ id: 'LOPD_TEXT_15' })}</li>
                <li>{intl.formatMessage({ id: 'LOPD_TEXT_16' })}</li>
                <li>{intl.formatMessage({ id: 'LOPD_TEXT_17' })}</li>
            </ul>
            <p className="title">{intl.formatMessage({ id: 'LOPD_TEXT_18' })}</p>
            <p>{intl.formatMessage({ id: 'LOPD_TEXT_19' })}</p>
            <p className="title">{intl.formatMessage({ id: 'LOPD_TEXT_20' })}</p>
            <p>{intl.formatMessage({ id: 'LOPD_TEXT_21' })}</p>
            <p>{intl.formatMessage({ id: 'LOPD_TEXT_22' })}</p>
            <p>{intl.formatMessage({ id: 'LOPD_TEXT_23' })}</p>
            <p>{intl.formatMessage({ id: 'LOPD_TEXT_24' })}</p>
            <p>{intl.formatMessage({ id: 'LOPD_TEXT_25' })}</p>
            <p className="final">
                <a
                    href="https://www.coronavirusmakers.org/politica-privacidad/"
                    target="_blank"
                    rel="noopener noreferrer"
                >
                    {intl.formatMessage({ id: 'LOPD_TITLE' })}
                </a>
            </p>
        </div>
    );
};

/*


*/
