import React, { useEffect } from 'react';
import ReactDOM from 'react-dom';
import './index.scss';
import { motion, AnimatePresence } from 'framer-motion';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faTimes } from '@fortawesome/free-solid-svg-icons';

interface IModalChildProps {
    close: () => void;
    open: boolean;
    children: any;
}

const Modal = ({ children, open, close }: IModalChildProps & any) => {
    const variants = {
        hidden: {
            opacity: 0,
            top: '120vh',
            transition: {
                duration: 0.3,
                ease: 'easeOut',
            },
        },
        visible: {
            opacity: 1,
            top: 0,
            transition: {
                duration: 0.3,
                ease: 'easeOut',
            },
        },
    };

    useEffect(() => {
        let isSubscribed = true;
        if (open && isSubscribed) {
            window.scrollTo(0, 0);
            window.onpopstate = () => {
                window.history.go(1);
                if (!!close) {
                    close();
                }
            };
        } else {
            window.onpopstate = () => {};
        }
        return () => {
            isSubscribed = false;
        };
    }, [open, close]);

    return ReactDOM.createPortal(
        <>
            {!!open && (
                <AnimatePresence initial={true}>
                    <motion.div
                        animate={'visible'}
                        initial={'hidden'}
                        exit="hidden"
                        variants={variants}
                        className={`modal ${!open ? 'hidden' : ''}`}
                    >
                        <FontAwesomeIcon size="lg" icon={faTimes} className="closeIcon" onClick={() => close()} />
                        {children}
                    </motion.div>
                </AnimatePresence>
            )}
        </>,
        document.body
    );
};

export default Modal;
