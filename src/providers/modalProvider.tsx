import React, { useState } from 'react';

interface IShowModal {
    active: boolean;
    content: JSX.Element;
}
interface IContext {
    showModal: IShowModal;
    setShowModal: React.Dispatch<React.SetStateAction<IShowModal>>;
    resetModal: () => void;
}

const initialState: IContext = {
    showModal: { active: false, content: <React.Fragment /> },
    setShowModal: () => ({}),
    resetModal: () => ({})
};

export const ModalContext = React.createContext(initialState);

export const ModalProvider = (props: any) => {
    const [showModal, setShowModal] = useState({ active: false, content: <React.Fragment /> });

    const resetModal = () => setShowModal({ active: false, content: <React.Fragment /> });
    return (
        <ModalContext.Provider
            value={{
                showModal,
                setShowModal,
                resetModal
            }}
        >
            {props.children}
        </ModalContext.Provider>
    );
};
