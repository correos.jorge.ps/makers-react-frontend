import React, { useState } from 'react';
export enum LanguagesType {
    es = 'es',
}
interface IContext {
    language: LanguagesType;
    setLanguage: React.Dispatch<React.SetStateAction<LanguagesType>>;
}

const initialState: IContext = {
    language: LanguagesType.es,
    setLanguage: () => ({}),
};

export const LanguageContext = React.createContext(initialState);

export const LanguageProvider = (props: any) => {
    const [language, setLanguage] = useState<LanguagesType>(LanguagesType.es);

    return (
        <LanguageContext.Provider
            value={{
                language,
                setLanguage,
            }}
        >
            {props.children}
        </LanguageContext.Provider>
    );
};
