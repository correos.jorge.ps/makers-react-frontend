import React, { useState } from 'react';

interface IContext {
    color: 'red' | 'green';
    setColor: React.Dispatch<React.SetStateAction<'red' | 'green'>>;
    notificationMessage: string;
    setNotificationMessage: React.Dispatch<React.SetStateAction<string>>;
}

const initialState: IContext = {
    color: 'green',
    setColor: () => ({}),
    notificationMessage: '',
    setNotificationMessage: () => ({}),
};

export const NotificationContext = React.createContext(initialState);

export const NotificationProvider = (props: any) => {
    const [color, setColor] = useState<'red' | 'green'>('green');
    const [notificationMessage, setNotificationMessage] = useState('');

    return (
        <NotificationContext.Provider
            value={{
                color,
                setColor,
                notificationMessage,
                setNotificationMessage,
            }}
        >
            {props.children}
        </NotificationContext.Provider>
    );
};
