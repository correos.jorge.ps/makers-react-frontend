import React from 'react';
import { Switch, Route, RouteProps, Redirect } from 'react-router-dom';
import { ModalProvider } from '../providers/modalProvider';

import { LoginPage } from '../pages/loginPage';
import { RegisterPage } from '../pages/registerPage';
import { CreatePersonPage } from '../pages/createPersonPage';
import { MainMakerInventoryPage } from '../pages/maker/inventory/mainMakerInventoryPage';
import { CreateMakerInventoryPage } from '../pages/maker/inventory/createMakerInventoryPage';
import { SummaryMakerInventoryPage } from '../pages/maker/inventory/summaryMakerInventoryPage';
import { MainMakerMaterialPage } from '../pages/maker/material/mainMakerMaterialPage';
import { CreateMakerMaterialPage } from '../pages/maker/material/createMakerMaterialPage';
import { SummaryMakerMaterialPage } from '../pages/maker/material/summaryMakerMaterialPage';
import { PrivateRoute } from './privateRoute';
import { UserDataProvider } from '../providers/userDataProvider';
import { CheckAndRedirectPage } from '../pages/checkAndRedirectPage';
import { MainHealthRequestPage } from '../pages/health/request/mainHealthRequestPage';
import { CreateHealthRequestPage } from '../pages/health/request/createHealthRequestPage';
import { SummaryHealthRequestPage } from '../pages/health/request/summaryHealthRequestPage';

export const Routes = () => {
    return (
        <ModalProvider>
            <Switch>
                <Route exact={true} path="/login" component={(props: RouteProps) => <LoginPage {...props} />} />
                <Route exact={true} path="/register" component={(props: RouteProps) => <RegisterPage {...props} />} />
                <UserDataProvider>
                    <Route
                        exact={true}
                        path="/"
                        component={(props: RouteProps) => <CheckAndRedirectPage {...props} />}
                    />

                    <PrivateRoute
                        exact={false}
                        path="/person"
                        component={(props: RouteProps) => <CreatePersonPage {...props} />}
                    />

                    <PrivateRoute
                        exact={true}
                        path="/maker/inventory/main"
                        component={(props: RouteProps) => <MainMakerInventoryPage {...props} />}
                    />
                    <PrivateRoute
                        exact={true}
                        path="/maker/inventory/create"
                        component={(props: RouteProps) => <CreateMakerInventoryPage {...props} />}
                    />
                    <PrivateRoute
                        exact={true}
                        path="/maker/inventory/success"
                        component={(props: RouteProps) => <SummaryMakerInventoryPage {...props} />}
                    />
                    <PrivateRoute
                        exact={true}
                        path="/maker/request/main"
                        component={(props: RouteProps) => <MainMakerMaterialPage {...props} />}
                    />
                    <PrivateRoute
                        exact={true}
                        path="/maker/request/create"
                        component={(props: RouteProps) => <CreateMakerMaterialPage {...props} />}
                    />
                    <PrivateRoute
                        exact={true}
                        path="/maker/request/success"
                        component={(props: RouteProps) => <SummaryMakerMaterialPage {...props} />}
                    />
                    <PrivateRoute
                        exact={true}
                        path="/health/request/main"
                        component={(props: RouteProps) => <MainHealthRequestPage {...props} />}
                    />
                    <PrivateRoute
                        exact={true}
                        path="/health/request/create"
                        component={(props: RouteProps) => <CreateHealthRequestPage {...props} />}
                    />
                    <PrivateRoute
                        exact={true}
                        path="/health/request/success"
                        component={(props: RouteProps) => <SummaryHealthRequestPage {...props} />}
                    />
                </UserDataProvider>
            </Switch>
            <Redirect exact={true} from="/*" to={`/`} />
        </ModalProvider>
    );
};
