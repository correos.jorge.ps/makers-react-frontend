import * as React from 'react';
import { Route, RouteComponentProps, RouteProps, useHistory } from 'react-router-dom';
import { Spinner } from '../components/spinner';
import { AuthConsumer } from '../providers/authProvider';

type RouteComponent = React.StatelessComponent<RouteComponentProps<{}>> | React.ComponentClass<any>;

export const PrivateRoute: React.StatelessComponent<RouteProps> = ({ component, ...rest }) => {
    const histoy = useHistory();
    const renderFn = (Component?: RouteComponent) => (props: RouteProps) => (
        <AuthConsumer>
            {({ isAuthenticated }: any) => {
                if (!!Component && isAuthenticated()) {
                    // @ts-ignore
                    return <Component {...props} />;
                } else {
                    histoy.push('/login');
                    return <Spinner />;
                }
            }}
        </AuthConsumer>
    );

    return <Route {...rest} render={renderFn(component)} />;
};
