import React from 'react';
import { RouteProps, useHistory } from 'react-router-dom';
import { useIntl } from 'react-intl';
import { useUserData } from '../../../../customHooks/useUserData';
import { useMainPage } from '../../../../customHooks/pageHooks/useMainPage';
import { RequestService } from '../../../../services/requestService';
import { MainRequestContainer } from '../../../../containers/containerPages/mainRequestContainer';
import { RoleType } from '../../../../models';
import { UrlManager, EntityType } from '../../../../helpers/helpers';

export const MainMakerMaterialPage = (props: RouteProps) => {
    const intl = useIntl();
    const { getPersonPk } = useUserData();
    const history = useHistory();
    let personPK = getPersonPk();
    let newpersonPK: number = personPK !== null ? personPK : 0;
    const { list, getNext, nextUrl } = useMainPage({
        getItems: () => RequestService().getRequestByPersonId(newpersonPK),
    });

    return (
        <MainRequestContainer
            roleType={RoleType.Maker}
            pageTitle={intl.formatMessage({ id: 'MAIN_PAGE_MATERIAL_TITLE' })}
            action={() => history.push(UrlManager(RoleType.Maker, EntityType.Request).create)}
            requests={list}
            getNext={() => getNext()}
            nextUrl={nextUrl}
        />
    );
};
