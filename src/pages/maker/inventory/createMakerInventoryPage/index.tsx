import React from 'react';
import { RouteProps } from 'react-router-dom';
import { useCreatePage } from '../../../../customHooks/pageHooks/useCreatePage';
import { CreateContainer } from '../../../../containers/containerPages/createContainer';
import { ResourceType, RoleType } from '../../../../models';
import { InventoryService } from '../../../../services/inventoryService';
import { EntityType } from '../../../../helpers/helpers';

export const CreateMakerInventoryPage = (props: RouteProps) => {
    const { selected, resources, setSelected, showQuantityPopup, getNext, nextUrl } = useCreatePage({
        typeToFetch: ResourceType.FinalProduct,
        entity: EntityType.Inventory,
        role: RoleType.Maker,
        createAction: InventoryService().createInventory,
    });

    return (
        <CreateContainer
            role={RoleType.Maker}
            hasAlertInItems={true}
            resources={resources}
            setSelected={(id: string) => setSelected(id)}
            selected={selected}
            label={'CREATE_INVENTORY_PAGE_TITLE'}
            showQuantityPopup={() => showQuantityPopup()}
            getNext={() => getNext()}
            nextUrl={nextUrl}
        />
    );
};
