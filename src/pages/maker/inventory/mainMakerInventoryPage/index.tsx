import React from 'react';
import { RouteProps, useHistory } from 'react-router-dom';
import { useIntl } from 'react-intl';
import { useMainPage } from '../../../../customHooks/pageHooks/useMainPage';
import { MainInventoryContainer } from '../../../../containers/containerPages/mainInventoryContainer';
import { ResourceType, RoleType } from '../../../../models';
import { InventoryService } from '../../../../services/inventoryService';
import { useUserData } from '../../../../customHooks/useUserData';
import { UrlManager, EntityType } from '../../../../helpers/helpers';

export const MainMakerInventoryPage = (props: RouteProps) => {
    const intl = useIntl();
    const { getPersonPk } = useUserData();
    const history = useHistory();
    let personPK = getPersonPk();
    let newpersonPK: number = personPK !== null ? personPK : 0;
    const { list, getNext, nextUrl } = useMainPage({
        getItems: () => InventoryService().getInventoryByPersonId(newpersonPK, ResourceType.FinalProduct),
    });

    return (
        <MainInventoryContainer
            pageTitle={intl.formatMessage({ id: 'MAIN_PAGE_TITLE' })}
            action={() => history.push(UrlManager(RoleType.Maker, EntityType.Inventory).create)}
            inventory={list}
            getNext={() => getNext()}
            nextUrl={nextUrl}
        />
    );
};
