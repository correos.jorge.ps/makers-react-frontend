import React from 'react';
import { RouteProps, useHistory } from 'react-router-dom';
import { useSummaryPage } from '../../../../customHooks/pageHooks/useSuccessPage';
import { SummaryContainer } from '../../../../containers/containerPages/summaryContainer';
import { RoleType } from '../../../../models';
import { EntityType, UrlManager } from '../../../../helpers/helpers';

export const SummaryMakerInventoryPage = (props: RouteProps) => {
    const history = useHistory();
    const { data, changeStatus, removeItem, isJustCreated } = useSummaryPage({
        role: RoleType.Maker,
        entity: EntityType.Inventory,
        statusModifier: 1,
    });

    return (
        <SummaryContainer
            data={data}
            changeStatus={(shipping) => {
                changeStatus(shipping);
            }}
            removeItem={() => removeItem()}
            action={() => history.push(UrlManager(RoleType.Maker, EntityType.Inventory).main)}
            title={!!isJustCreated ? 'SUMMARY_INVENTORY_PAGE_TITLE' : null}
            subtitle="SUMMARY_INVENTORY_PAGE_SUBTITLE"
            instructions="CODE_PAGE_INSTRUCTIONS"
            buttonLabel="SUMMARY_INVENTORY_PAGE_GO_TO_MAIN"
            buttonConfirmLabel="LABEL_SET_SENDED"
        />
    );
};
