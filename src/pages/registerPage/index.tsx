import React, { useState } from 'react';
import logo from '../../assets/images/logoAuthPages.jpg';

import { RouteProps, useHistory } from 'react-router-dom';
import './index.scss';

import { IPostUser, UserService } from '../../services/userService';
import { AuthService } from '../../services/authService';
import { FormFactory, IInput } from '../../containers/formFactory';
import { AuthConsumer } from '../../providers/authProvider';
import { motion, AnimatePresence } from 'framer-motion';
import { useIntl } from 'react-intl';
import { DataProtectionModal } from '../../containers/modal/dataProtectionModal';
import { useModal } from '../../customHooks/useModal';
import Modal from '../../containers/modal';
import { CheckBox } from '../../components/checkbox';
import useNotification from '../../customHooks/useNotification';
import { NotificationBar } from '../../components/notificationBar/notificationBar';

export const RegisterPage = (props: RouteProps) => {
    let history = useHistory();
    const intl = useIntl();
    const { setNotificationMessageDelayed, notificationMessage, color } = useNotification();

    const [acceptedPolicy, setAcceptedPolicy] = useState<boolean | null>(null);
    const { setShowModal, resetModal, showModal } = useModal();
    const variants = {
        hidden: {
            opacity: 0,
            transition: {
                duration: 0.3,
                ease: 'easeOut',
            },
        },
        visible: {
            opacity: 1,
            transition: {
                duration: 0.3,
                ease: 'easeOut',
            },
        },
    };

    const items: IInput[] = [
        {
            name: 'email',
            type: 'email',
            label: 'LABEL_EMAIL',
            placeholder: 'LABEL_EMAIL',
            component: 'Input',
            required: true,
        },
        {
            name: 'username',
            type: 'text',
            label: 'LABEL_USERNAME',
            placeholder: 'LABEL_USERNAME',
            component: 'Input',
            required: true,
        },
        {
            name: 'password',
            type: 'password',
            label: 'LABEL_PASSWORD',
            placeholder: 'LABEL_PASSWORD',
            component: 'Input',
            required: true,
        },
    ];

    const submitRegister = (
        data: IPostUser,
        setAccessToken: (value: string) => void,
        setRefreshToken: (value: string) => void
    ) => {
        if (!acceptedPolicy) {
            return new Promise((resolve) => {
                setNotificationMessageDelayed('red', 'LABEL_ACCEPT_POLICY');
                resolve();
            });
        }
        return UserService()
            .create(data)
            .then((response) => {
                const { email, ...rest } = data;
                AuthService()
                    .createJWT(rest)
                    .then((tokens) => {
                        setAccessToken(tokens.access);
                        setRefreshToken(tokens.refresh);
                        history.push('/person');
                    });
            });
    };

    const showDataProtectionModal = () => {
        let modalConfig = {
            active: true,
            content: <DataProtectionModal onConfirm={() => resetModal()} />,
        };
        setShowModal(modalConfig);
    };

    return (
        <AnimatePresence initial={false}>
            <NotificationBar notificationMessage={notificationMessage} color={color} />

            <motion.main
                className="registerContainer"
                animate={'visible'}
                initial={'hidden'}
                exit="hidden"
                variants={variants}
            >
                <div className="authLinks">
                    <span onClick={() => history.push('/login')}>{intl.formatMessage({ id: 'LABEL_ENTER' })}</span>
                    <span> | </span>
                    <span className="selected" onClick={() => history.push('/register')}>
                        {intl.formatMessage({ id: 'LABEL_REGISTER' })}
                    </span>
                </div>
                <div className="margin-l" />
                <div className="registerFormContainer">
                    <AuthConsumer>
                        {({ setAccessToken, setRefreshToken }: any) => (
                            <FormFactory
                                items={items}
                                submitAction={(formData) => submitRegister(formData, setAccessToken, setRefreshToken)}
                                submitLabel="LABEL_CONTINUE"
                            >
                                <img src={logo} className="logo" alt="logo" />
                                <div className="margin-l" />
                                {acceptedPolicy === false && (
                                    <span className="errorPolicy">
                                        {intl.formatMessage({ id: 'LABEL_FORCE_TO_ACCEPT' })}
                                    </span>
                                )}
                            </FormFactory>
                        )}
                    </AuthConsumer>
                    <div className="link">
                        <p onClick={() => showDataProtectionModal()}>Politica de privacidad de datos</p>
                        <CheckBox
                            value={!!acceptedPolicy}
                            onChange={() => setAcceptedPolicy((current) => !current)}
                            label="Acepto la politica de privacidad"
                        />
                    </div>
                </div>
                <Modal open={showModal.active} close={() => resetModal()}>
                    {showModal.content}
                </Modal>
            </motion.main>
        </AnimatePresence>
    );
};
