import React from 'react';
import { RouteProps, useHistory } from 'react-router-dom';
import { useSummaryPage } from '../../../../customHooks/pageHooks/useSuccessPage';
import { SummaryContainer } from '../../../../containers/containerPages/summaryContainer';
import { EntityType, UrlManager } from '../../../../helpers/helpers';
import { RoleType } from '../../../../models';

export const SummaryHealthRequestPage = (props: RouteProps) => {
    const history = useHistory();
    const { data, changeStatus, removeItem, isJustCreated } = useSummaryPage({
        role: RoleType.HealthCare,
        entity: EntityType.Request,
        statusModifier: 2,
    });

    return (
        <SummaryContainer
            removeItem={() => removeItem()}
            changeStatus={(shipping) => changeStatus(shipping)}
            data={data}
            hasQr={false}
            action={() => history.push(UrlManager(RoleType.HealthCare, EntityType.Request).main)}
            title={!!isJustCreated ? 'SUMMARY_MATERIAL_PAGE_TITLE' : null}
            subtitle="SUMMARY_MATERIAL_PAGE_SUBTITLE"
            instructions="CODE_MATERIAL_PAGE_INSTRUCTIONS"
            buttonLabel="SUMMARY_MATERIAL_PAGE_GO_TO_MAIN"
            buttonConfirmLabel="LABEL_SET_RECIEVED"
        />
    );
};
