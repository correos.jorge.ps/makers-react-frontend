import React from 'react';
import { RouteProps } from 'react-router-dom';
import { useCreatePage } from '../../../../customHooks/pageHooks/useCreatePage';
import { CreateContainer } from '../../../../containers/containerPages/createContainer';
import { ResourceType, RoleType } from '../../../../models';
import { RequestService } from '../../../../services/requestService';
import { EntityType } from '../../../../helpers/helpers';

export const CreateHealthRequestPage = (props: RouteProps) => {
    const { selected, resources, setSelected, showQuantityPopup, getNext, nextUrl } = useCreatePage({
        typeToFetch: ResourceType.FinalProduct,
        entity: EntityType.Request,
        role: RoleType.HealthCare,
        createAction: RequestService().createRequest,
    });

    return (
        <CreateContainer
            role={RoleType.HealthCare}
            hasAlertInItems={false}
            resources={resources}
            setSelected={(id: string) => setSelected(id)}
            selected={selected}
            label={'CREATE_INVENTORY_PAGE_TITLE'}
            showQuantityPopup={() => showQuantityPopup()}
            getNext={() => getNext()}
            nextUrl={nextUrl}
        />
    );
};
