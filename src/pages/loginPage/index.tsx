import React, { useEffect, useState } from 'react';
import logo from '../../assets/images/logoAuthPages.jpg';

import { RouteProps, useHistory } from 'react-router-dom';
import './index.scss';
import { AuthService } from '../../services/authService';
import { FormFactory, IInput } from '../../containers/formFactory';
import { AuthConsumer } from '../../providers/authProvider';
import { motion, AnimatePresence } from 'framer-motion';
import { useIntl } from 'react-intl';
interface IPostToken {
    username: string;
    password: string;
}
export const LoginPage = (props: RouteProps) => {
    let history = useHistory();
    const intl = useIntl();
    const [isVisible, setIsVisible] = useState(false);
    const variants = {
        hidden: {
            opacity: 0,
            transition: {
                duration: 0.3,
                ease: 'easeOut',
            },
        },
        visible: {
            opacity: 1,
            transition: {
                duration: 0.3,
                ease: 'easeOut',
            },
        },
    };

    const items: IInput[] = [
        {
            name: 'username',
            type: 'text',
            label: 'LABEL_USERNAME',
            placeholder: 'LABEL_USERNAME',
            component: 'Input',
            required: true,
        },
        {
            name: 'password',
            type: 'password',
            label: 'LABEL_PASSWORD',
            placeholder: 'LABEL_PASSWORD',
            component: 'Input',
            required: true,
        },
    ];

    const submitLogin = (
        data: IPostToken,
        setAccessToken: (value: string) => void,
        setRefreshToken: (value: string) => void
    ) => {
        return AuthService()
            .createJWT(data)
            .then((tokens) => {
                setAccessToken(tokens.access);
                setRefreshToken(tokens.refresh);
                history.push('/');
            });
    };

    useEffect(() => {
        let jwt = localStorage.getItem('access');

        if (!!jwt) {
            AuthService()
                .verifyJWT({ token: jwt })
                .then(() => {
                    history.push('/');
                })
                .catch((e) => {
                    const refresh = localStorage.getItem('refresh');
                    if (!!refresh) {
                        AuthService()
                            .refreshJWT({ refresh })
                            .then(() => {
                                history.push('/');
                            })
                            .catch((e) => {
                                setIsVisible(true);
                                localStorage.clear();
                            });
                    }
                });
        } else {
            setIsVisible(true);
        }
    }, [history]);

    if (!isVisible) {
        return <div></div>;
    }

    return (
        <AnimatePresence initial={false}>
            <motion.main
                className="loginContainer"
                animate={'visible'}
                initial={'hidden'}
                exit="hidden"
                variants={variants}
            >
                <div className="authLinks">
                    <span className="selected" onClick={() => history.push('/login')}>
                        {intl.formatMessage({ id: 'LABEL_ENTER' })}
                    </span>
                    <span> | </span>
                    <span onClick={() => history.push('/register')}>
                        {intl.formatMessage({ id: 'LABEL_REGISTER' })}
                    </span>
                </div>
                <div className="margin-l" />
                <div className="loginFormContainer">
                    <AuthConsumer>
                        {({ setAccessToken, setRefreshToken }: any) => {
                            return (
                                <FormFactory
                                    items={items}
                                    submitAction={(formData) => submitLogin(formData, setAccessToken, setRefreshToken)}
                                    submitLabel="LABEL_CONTINUE"
                                >
                                    <img src={logo} className="logo" alt="logo" />
                                    <div className="margin-l" />
                                </FormFactory>
                            );
                        }}
                    </AuthConsumer>
                </div>
            </motion.main>
        </AnimatePresence>
    );
};
