import React, { useState, useEffect } from 'react';
import logo from '../../assets/images/logoAuthPages.jpg';

import { RouteProps, useHistory } from 'react-router-dom';
import { Layout } from '../../containers/layout';
import './index.scss';
import { FormFactory, IInput } from '../../containers/formFactory';
import { PersonService } from '../../services/personService';
import { useUserData } from '../../customHooks/useUserData';
import { IPerson, IRole, RoleType } from '../../models';
import { CheckBox } from '../../components/checkbox';
import { UrlManager } from '../../helpers/helpers';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faTimesCircle, faChevronCircleLeft } from '@fortawesome/free-solid-svg-icons';
import useNotification from '../../customHooks/useNotification';

export const CreatePersonPage = (props: RouteProps) => {
    const { setPerson, person } = useUserData();
    const [roles, setRoles] = useState<IRole[]>([]);
    const [types, setTypes] = useState<IRole[]>([]);
    const { setNotificationMessageDelayed } = useNotification();
    const [showPrinterItems, setShowPrinterItems] = useState(false);
    const [formActive, setFormActive] = useState(0);
    let history = useHistory();
    const userFormItems: IInput[] = [
        {
            name: 'name',
            type: 'text',
            label: 'LABEL_NAME',
            placeholder: 'LABEL_NAME',
            component: 'Input',
            required: true,
        },
        {
            name: 'phone',
            type: 'tel',
            pattern: '[0-9]{9}',
            label: 'LABEL_PHONE',
            placeholder: 'LABEL_FORMAT_PHONE',
            component: 'Input',
        },
        {
            name: 'telegram_nick',
            type: 'text',
            label: 'LABEL_TELEGRAM_NICK',
            placeholder: 'PLACEHOLDER_TELEGRAM_NICK',
            component: 'Input',
        },
        {
            name: 'role',
            type: 'select',
            classList: !person || (person && !person.pk) ? '' : 'hidden',
            label: 'LABEL_ROLES',
            placeholder: 'LABEL_SELECT',
            component: 'SelectInput',
            required: true,
            options: roles,
        },
    ];

    const roleFromItems: IInput[] = [
        {
            name: 'role_disambiguation',
            type: 'select',
            classList: person && !person.pk ? '' : 'hidden',
            label: 'LABEL_ROLE',
            placeholder: 'LABEL_ROLE',
            component: 'SelectInput',
            required: true,
            options: types,
        },
        {
            name: 'region',
            type: 'search',
            label: 'LABEL_REGION',
            placeholder: 'LABEL_REGION_SEARCH',
            component: 'RegionInput',
            required: true,
            defaultValue: person?.region_object,
        },
        {
            name: 'city',
            classList: 'hidden',
            type: 'text',
            readOnly: true,
            label: 'LABEL_CITY',
            placeholder: 'LABEL_CITY',
            component: 'Input',
            required: true,
        },
        {
            name: 'cp',
            classList: 'hidden',
            readOnly: true,
            type: 'string',
            label: 'LABEL_CP',
            placeholder: 'LABEL_CP',
            component: 'Input',
            required: true,
        },
        {
            name: 'address',
            type: 'text',
            label: 'LABEL_STREET',
            placeholder: 'LABEL_STREET',
            component: 'Input',
            required: true,
        },
        {
            name: 'number',
            type: 'text',
            label: 'LABEL_NUMBER',
            placeholder: 'LABEL_NUMBER',
            component: 'Input',
            required: true,
        },
        {
            name: 'province',
            type: 'text',
            label: 'LABEL_PROVINCE',
            placeholder: 'LABEL_PROVINCE',
            component: 'Input',
            required: true,
        },
        {
            name: 'country',
            type: 'string',
            label: 'LABEL_COUNTRY',
            placeholder: 'LABEL_COUNTRY',
            component: 'Input',
            required: true,
        },
    ];
    const makerForm: IInput[] = [
        {
            name: 'machine_count',
            type: 'number',
            classList: person && person.machine_models && person.machine_models !== '' ? '' : 'hidden',
            label: 'LABEL_MACHINE_COUNT',
            placeholder: 'LABEL_MACHINE_COUNT',
            component: 'Input',
        },
        {
            name: 'machine_models',
            classList: person && person.machine_models && person.machine_models !== '' ? '' : 'hidden',
            type: 'text',
            label: 'LABEL_MACHINE_MODELS',
            placeholder: 'LABEL_MACHINE_MODELS',
            component: 'Input',
        },
    ];

    useEffect(() => {
        let isSubscribed = true;
        PersonService()
            .getRoles()
            .then((response) => {
                if (isSubscribed) {
                    setRoles(
                        response.results.filter((role) => role.pk === RoleType.Maker || role.pk === RoleType.HealthCare)
                    );
                }
            });
        return () => {
            isSubscribed = false;
        };
    }, []);

    useEffect(() => {
        if (!!person && person.role !== null) {
            PersonService()
                .getRolesTypes(person.role)
                .then((result) => {
                    setTypes(result.results);
                });
        }

        let value = !!person && !!person.machine_models;
        setShowPrinterItems(value);
    }, [person]);

    const showPrintItems = () => {
        setShowPrinterItems((current) => !current);
        if (!!document) {
            //@ts-ignore
            document.getElementById('machine_models').classList.toggle('hidden');
            //@ts-ignore
            document.getElementById('machine_count').classList.toggle('hidden');
        }
    };

    const submitFirstForm = (data: IPerson) => {
        return new Promise((resolve) => {
            setPerson(data);
            setFormActive(1);

            resolve();
        });
    };
    const createPerson = (data: IPerson) => {
        if (!setShowPrinterItems) {
            data = { ...data, ...{ machine_count: 0, machine_models: null } };
        }
        if (person && person.pk) {
            return PersonService()
                .updatePerson(data, String(person.pk))
                .then((response) => {
                    setPerson(response);
                    setNotificationMessageDelayed('green');
                    history.push(UrlManager(person.role).main);
                });
        }
        return PersonService()
            .createPerson(data)
            .then((response) => {
                setPerson(response);
                setNotificationMessageDelayed('green');
                history.push(UrlManager(response.role).main);
            });
    };

    const submitRole = (data: IPerson) => {
        if (data.role != RoleType.Maker) {
            return new Promise((resolve) => {
                createPerson(data);
                resolve();
            });
        }
        return new Promise((resolve) => {
            setPerson(data);
            setFormActive(2);
            resolve();
        });
    };

    return (
        <Layout hasHeader={false}>
            <div className="createPersonPage">
                <div className="iconsContainer">
                    {person && person.pk && (
                        <FontAwesomeIcon
                            className="closeIcon"
                            size="lg"
                            icon={faTimesCircle}
                            onClick={() => history.goBack()}
                        />
                    )}
                    {formActive > 0 && (
                        <FontAwesomeIcon
                            onClick={() => {
                                setFormActive(formActive - 1);
                            }}
                            className="backIcon"
                            size="lg"
                            icon={faChevronCircleLeft}
                        />
                    )}
                </div>
                <img src={logo} className="logo" alt="logo" />
                <div className="margin-xl" />
                {formActive === 0 ? (
                    <FormFactory
                        defaultValues={person}
                        items={userFormItems}
                        submitAction={(formData) => submitFirstForm(formData)}
                        submitLabel="LABEL_CONTINUE"
                    >
                        <span></span>
                    </FormFactory>
                ) : formActive === 1 ? (
                    <FormFactory
                        defaultValues={person}
                        items={roleFromItems}
                        submitAction={(formData) => submitRole(formData)}
                        submitLabel="LABEL_CONTINUE"
                    >
                        <span></span>
                    </FormFactory>
                ) : (
                    <>
                        <FormFactory
                            defaultValues={person}
                            items={makerForm}
                            submitAction={(formData) => createPerson(formData)}
                            submitLabel="LABEL_CONTINUE"
                        >
                            <CheckBox
                                value={showPrinterItems}
                                onChange={() => showPrintItems()}
                                label="Tengo Impresora 3d"
                            />
                        </FormFactory>
                    </>
                )}
            </div>
        </Layout>
    );
};
