import React, { useState, useEffect, useCallback } from 'react';
import { Input } from '../input';
import debounce from 'lodash/debounce';
import './index.scss';
import { RegionService } from '../../services/regionService';
import { Spinner } from '../spinner';
import { FormFactory, IInput } from '../../containers/formFactory';
interface IProps {
    name: string;
    label: string;
    placeholder: string;
    value: any;
    defaultValue?: IRegions;
    onChange: (e: any) => void;
}

interface IRegions {
    pk: number;
    name: string;
    postal_code: number;
}

export const RegionInput = ({ name, label, placeholder, value, defaultValue, onChange }: IProps) => {
    const [options, setOptions] = useState<IRegions[]>([]);
    const [isLoading, setIsLoading] = useState(false);
    const [textValue, setTextValue] = useState('');
    const [cp, setCpValue] = useState<number | null>(null);

    const [isOpen, setIsOpen] = useState(false);
    useEffect(() => {
        if (defaultValue) {
            setTextValue(defaultValue.name);
            setCpValue(defaultValue.postal_code);
            debounceFunc(defaultValue.name);
        }
    }, [defaultValue]);

    const debounceFunc = (val: string) => {
        setIsLoading(true);
        RegionService()
            .getRegions(val)
            .then((response: any) => {
                setOptions(response.results);
                setIsLoading(false);
            });
    };

    const handler = useCallback(debounce(debounceFunc, 400), []);

    const onChangeLocal = (data: string) => {
        setTextValue(data);
        if (data.length > 2) {
            handler(data);
        }
    };

    const selectOption = (value: IRegions) => {
        setIsOpen(false);
        setTextValue(value.name);
        setCpValue(value.postal_code);
        onChange({
            target: {
                region: value.pk,
                city: value.name,
                cp: value.postal_code,
            },
        });
    };
    const config: IInput[] = [
        {
            name: 'city',
            type: 'text',
            readOnly: true,
            label: 'LABEL_CITY',
            placeholder: 'LABEL_CITY',
            component: 'Input',
            required: true,
        },
        {
            name: 'cp',
            readOnly: true,
            type: 'string',
            label: 'LABEL_CP',
            placeholder: 'LABEL_CP',
            component: 'Input',
            required: true,
        },
    ];
    return (
        <div className="formGroup search">
            <div className="inputContainerSearch">
                <Input
                    name={name}
                    type="text"
                    onFocus={() => setIsOpen((current) => true)}
                    label={label}
                    placeholder={placeholder}
                    onChange={(e) => onChangeLocal(e.target.value)}
                    value={textValue}
                />
                <div className="regionListContainer">
                    {isLoading && <Spinner />}
                    {isOpen && textValue.length > 3 && (
                        <ul>
                            {options.map((option) => (
                                <li onClick={() => selectOption(option)} key={option.pk}>
                                    {option.name}-{option.postal_code}
                                </li>
                            ))}
                        </ul>
                    )}
                </div>
            </div>
            <div className="margin-s" />
            <FormFactory defaultValues={{ cp: cp, city: textValue }} items={config} submitLabel="LABEL_CONTINUE">
                <div />
            </FormFactory>
            <div className="margin-m"> </div>
        </div>
    );
};
