import React from 'react';
import './index.scss';
interface IProps {
    type: string;
    label: string;
    placeholder: string;
    onChange: (e: any) => void;
    name: string;
    value?: string | number;
    error?: string;
    required?: boolean;
    options?: any;
    pattern?: string;
    classList?: string;
    onFocus?: () => void;
    onBlur?: () => void;
    readOnly?: boolean;
}

export const Input = ({
    name,
    type,
    label,
    placeholder,
    onChange,
    value = '',
    error,
    required = false,
    pattern,
    classList,
    onFocus,
    onBlur,
    readOnly = false,
}: IProps) => {
    return (
        <div className={`${classList ? classList : ''} formGroup`} id={name}>
            <label htmlFor={name}>
                <span>{label}</span>
            </label>
            <input
                required={required}
                name={name}
                autoComplete="off"
                onChange={(e) => onChange(e)}
                type={type}
                onFocus={!!onFocus ? () => onFocus() : () => ({})}
                disabled={readOnly}
                onBlur={!!onBlur ? () => onBlur() : () => ({})}
                pattern={pattern}
                value={value}
                placeholder={placeholder}
            />
            {error && (
                <div className="error">
                    <span>{error}</span>
                </div>
            )}
        </div>
    );
};
