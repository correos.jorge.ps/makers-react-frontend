import React from 'react'
import "./index.scss"
interface IProps {
    children: any
    isSelected?: boolean,
    action?: () => void
}
export const Card = ({ children, isSelected = false, action = () => ({}) }: IProps) => {
    return (
        <li className={`card ${isSelected ? "selected" : ""}`} onClick={() => action()}>
            {children}
        </li>
    )
}
