import React from 'react';
import { IShipping } from '../../../models';
import { Card } from '../index';
import { InfoItem } from '../../infoItem';
import { useIntl } from 'react-intl';
import './index.scss';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faCheckCircle } from '@fortawesome/free-solid-svg-icons';
import { Button } from '../../button';

interface IProps {
    item: IShipping;
    changeStatus: (arg: number) => void;
    buttonConfirmLabel: string;
}

export enum ShippingStatusType {
    Waiting = 0,
    Sended = 1,
    Recieved = 2,
    Programmed = 3,
    Cancelled = 4,
    InTransit = 5,
}
export const ShippingItem = ({ item, changeStatus, buttonConfirmLabel }: IProps) => {
    const intl = useIntl();

    const getShippingStatus = () => {
        let status = '';
        switch (item.status) {
            case ShippingStatusType.Waiting:
                status = 'Esperando';
                break;
            case ShippingStatusType.Sended:
                status = 'Enviado';
                break;
            case ShippingStatusType.Recieved:
                status = 'Recibido';
                break;
            case ShippingStatusType.Programmed:
                status = 'Programado';
                break;
            case ShippingStatusType.Cancelled:
                status = 'Cancelado';
                break;
            case ShippingStatusType.InTransit:
                status = 'En Tránsito';
                break;
            default:
                break;
        }
        return status;
    };
    return (
        <Card>
            <div className="info shippingItem">
                <InfoItem label={intl.formatMessage({ id: 'LABEL_QUANTITY' })} text={String(item.quantity)} />

                <InfoItem
                    label={intl.formatMessage({
                        id: 'LABEL_STATUS',
                    })}
                    text={getShippingStatus()}
                />

                <InfoItem
                    label={intl.formatMessage({
                        id: 'LABEL_TRANSPORTER',
                    })}
                    text={item.carrier_object ? String(item.carrier_object.name) : ''}
                />
                {(item.status === ShippingStatusType.Recieved || item.status === ShippingStatusType.Sended) && (
                    <FontAwesomeIcon size="lg" className="check" icon={faCheckCircle} />
                )}
                {(item.status === ShippingStatusType.InTransit || item.status === ShippingStatusType.Waiting) && (
                    <div className="full">
                        <Button type="primary small downloadQr " action={() => changeStatus(item.pk)}>
                            {intl.formatMessage({ id: buttonConfirmLabel })}
                        </Button>
                    </div>
                )}
            </div>
            <div className="margin-s"></div>
        </Card>
    );
};
