import React from 'react';
import './index.scss';
import { debounce } from 'lodash';
interface IProps {
    action: () => any;
    type?: string;
    children: any;
    isDisabled?: boolean;
}
export const Button = ({ action, children, type = 'primary', isDisabled = false }: IProps) => {
    const debouncedClick = debounce(action, 200);
    return (
        <button disabled={isDisabled} className={`${type}`} onClick={action ? () => debouncedClick() : () => {}}>
            {children}
        </button>
    );
};
