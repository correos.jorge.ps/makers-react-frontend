import { dataSource } from './dataSource';

interface IJwtResponse {
    refresh: string;
    access: string;
}
export const AuthService = () => {
    const createJWT = (data: { username: string; password: string }): Promise<IJwtResponse> => {
        return dataSource({
            config: {
                withToken: false,
            },
            data,
            method: 'POST',
            url: '/auth/jwt/create',
        }).then((response: IJwtResponse) => {
            return response;
        });
    };

    const verifyJWT = (data: { token: string }): Promise<IJwtResponse> => {
        return dataSource({
            config: {
                withToken: false,
            },
            data,
            method: 'POST',
            url: '/auth/jwt/verify',
        }).then((response: any) => {
            return response;
        });
    };

    const refreshJWT = (data: { refresh: string }): Promise<IJwtResponse> => {
        return dataSource({
            config: {
                withToken: false,
            },
            data,
            method: 'POST',
            url: '/auth/jwt/refresh',
        }).then((response: IJwtResponse) => {
            localStorage.setItem('access', response.access);
            return response;
        });
    };

    return {
        createJWT,
        verifyJWT,
        refreshJWT,
    };
};
