import { dataSource } from './dataSource';

interface IUser extends IPostUser {
    id: string;
}
export interface IPostUser {
    email: string;
    username: string;
    password: string;
}

export const UserService = () => {
    const create = (data: IPostUser): Promise<IUser> => {
        return dataSource({
            config: {
                withToken: false,
                handleError: true,
            },
            data,
            method: 'POST',
            url: '/auth/users/',
        });
    };

    return {
        create,
    };
};
