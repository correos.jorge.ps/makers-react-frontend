import { dataSource } from './dataSource';
import { IPerson, IRole, RoleType } from '../models';

export const PersonService = () => {
    const createPerson = (data: IPerson): Promise<IPerson> => {
        return dataSource({
            data,
            method: 'POST',
            url: '/logistica/basic/person/',
            config: {
                handleError: true,
            },
        });
    };

    const updatePerson = (data: IPerson, personId: string): Promise<IPerson> => {
        return dataSource({
            data,
            method: 'PUT',
            url: `/logistica/basic/person/${personId}`,
            config: {
                handleError: true,
            },
        });
    };

    const getPersonById = (personId: string) => {
        return dataSource({
            method: 'GET',
            url: `/logistica/basic/person/${personId}`,
            config: {
                handleError: true,
            },
        });
    };

    const getAllMyPersons = () => {
        return dataSource({
            method: 'GET',
            url: `/logistica/basic/person/`,
            config: {
                handleError: true,
            },
        });
    };

    const getRoles = (): Promise<{ results: IRole[] }> => {
        return dataSource({
            method: 'GET',
            url: `/logistica/basic/role/`,
        });
    };

    const getRolesTypes = (roleId: RoleType): Promise<{ results: IRole[] }> => {
        return dataSource({
            method: 'GET',
            url: `/logistica/basic/role/${roleId}/disambiguation`,
        });
    };

    return {
        createPerson,
        getPersonById,
        getRolesTypes,
        getAllMyPersons,
        updatePerson,
        getRoles,
    };
};
