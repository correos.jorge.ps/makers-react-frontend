import axios, { AxiosRequestConfig } from 'axios';

const client = axios.create({
    baseURL: process.env.REACT_APP_API_ROOT_URL,
});
export interface IDataSourceOptions extends AxiosRequestConfig {
    config?: {
        handleError?: boolean;
        withToken?: boolean;
    };
}

export const dataSource = ({ ...options }: IDataSourceOptions) => {
    const onSuccess = (response: any) => {
        return !!response ? response.data : Promise.reject(response);
    };

    const onError = (error: any) => {
        let eDescription = error.response;
        if (error.data && error.data.message) {
            eDescription = error.data.message;
            console.log(error.data.message);
        }

        return Promise.reject(eDescription);
    };

    const token = localStorage.getItem('access');
    options.headers =
        options.config && options.config.withToken === false
            ? options.headers
            : { ...options.headers, ...{ Authorization: `Bearer ${token}` } };

    if (options.config && options.config.handleError === false) {
        return client(options).then(onSuccess);
    }

    return client(options).then(onSuccess).catch(onError);
};
